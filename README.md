# SwaggerUI

Provide http.Handler with [SwaggerUI](https://github.com/swagger-api/swagger-ui) embed, and configurable.


## Example

```go
package main

import (
    "gopkg.org/swaggerui"
)

const (
    swaggerURL = "https://petstore.swagger.io/v2/swagger.json"
)

func main() {
	http.Handle("/doc/", http.StripPrefix("/doc/", swaggerui.Handler(swaggerURL)))
	if err := http.ListenAndServe(":8888", nil); err != nil {
		panic(err)
	}
}
```