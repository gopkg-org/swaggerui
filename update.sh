#!/bin/sh

# Github repository path
readonly REPOSITORY="swagger-api/swagger-ui"

# Get latest release information
RELEASE=$(curl -s "https://api.github.com/repos/${REPOSITORY}/releases/latest")
TAGNAME=$(jq -r .tag_name <<< "${RELEASE}")
VERSION=${TAGNAME#v}

# Print version downloaded
echo "Download swagger-api/swagger-ui version ${VERSION}"

# Download ZIP
curl -s -L -o swaggerui.zip "https://github.com/${REPOSITORY}/archive/refs/tags/${TAGNAME}.zip"

# Unzip dist folder
unzip -j -o swaggerui.zip "swagger-ui-${VERSION}/dist/*" -d "./dist"

# Update constant version
echo "package swaggerui\n\nconst Version = \"${VERSION}\"" > version.go

# Remove ZIP
rm swaggerui.zip
