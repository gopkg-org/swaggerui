package swaggerui

import (
	"bytes"
	"embed"
	"encoding/json"
	"fmt"
	"io"
	"io/fs"
	"net/http"
	"reflect"
	"strconv"
	"strings"

	"gopkg.org/generic"
)

//go:embed dist/*
var dist embed.FS

func Handler(swaggerURL string) http.Handler {
	return HandlerWithConfig(Config{
		URL:                    swaggerURL,
		DomID:                  "#swagger-ui",
		Layout:                 "StandaloneLayout",
		DeepLinking:            generic.Pointer(true),
		DisplayRequestDuration: generic.Pointer(true),
		Presets: []Item{
			"SwaggerUIBundle.presets.apis",
			"SwaggerUIStandalonePreset",
		},
		Plugins: []Item{
			"SwaggerUIBundle.plugins.DownloadUrl",
		},
	})
}

func HandlerWithConfig(config Config) http.Handler {
	return HandlerWithInitializer(config.Encode())
}

func HandlerWithInitializer(initializer []byte) http.Handler {
	rootFS, _ := fs.Sub(dist, "dist")
	return handler{fs: rootFS, initializer: initializer}
}

type handler struct {
	fs          fs.FS
	initializer []byte
}

func (h handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path == "swagger-initializer.js" {
		w.Header().Set("Content-Type", "text/javascript")

		if _, err := w.Write(h.initializer); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		return
	}

	http.FileServer(http.FS(h.fs)).ServeHTTP(w, r)
}

type Config struct {
	/*
		Core
	*/

	// URL to fetch external configuration document from.
	ConfigURL string `cfg:"configUrl"`

	// The ID of a DOM element inside which SwaggerUI will put its user interface.
	DomID string `cfg:"dom_id"`

	// The HTML DOM element inside which SwaggerUI will put its user interface. Overrides dom_id.
	DomNode string `cfg:"domNode"`

	// A JavaScript object describing the OpenAPI definition.
	// When used, the url parameter will not be parsed.
	// This is useful for testing manually-generated definitions without hosting them.
	Spec Object `cfg:"spec"`

	// The URL pointing to API definition (normally swagger.json or swagger.yaml).
	// Will be ignored if urls or spec is used.
	URL string `cfg:"url"`

	// An array of API definition objects ([{url: "<url1>", name: "<name1>"},{url: "<url2>", name: "<name2>"}])
	// used by Topbar plugin. When used and Topbar plugin is enabled, the url parameter will not be parsed.
	// Names and URLs must be unique among all items in this array, since they're used as identifiers.
	URLS []URL `cfg:"urls"`

	// Enables overriding configuration parameters via URL search params.
	QueryConfigEnabled *bool `cfg:"queryConfigEnabled"`

	/*
		Plugin system
		Read more about the plugin system in the Customization documentation.
		@URL: https://github.com/swagger-api/swagger-ui/blob/master/docs/customization/overview.md
	*/

	// The name of a component available via the plugin system to use as the top-level layout for Swagger UI.
	Layout string `cfg:"layout"`

	// A Javascript object to configure plugin integration and behaviors (see below).
	PluginsOptions Object `cfg:"pluginsOptions"`

	// An array of plugin functions to use in Swagger UI.
	Plugins []Item `cfg:"plugins"`

	// An array of presets to use in Swagger UI. Usually, you'll want to include ApisPreset if you use this option.
	Presets []Item `cfg:"presets"`

	/*
		Plugins options
	*/

	// Control behavior of plugins when targeting the same component with wrapComponent.
	// - legacy (default) : last plugin takes precedence over the others
	// - chain : chain wrapComponents when targeting the same core component, allowing multiple
	//           plugins to wrap the same component
	// @Available: ["legacy", "chain"]
	PluginLoadType string `cfg:"pluginLoadType"`

	/*
		Display
	*/

	// If set to true, enables deep linking for tags and operations.
	// See the Deep Linking documentation for more information.
	// @URL: https://swagger.io/docs/usage/deep-linking.md
	DeepLinking *bool `cfg:"deepLinking"`

	// Controls the display of operationId in operations list. The default is false.
	DisplayOperationID *bool `cfg:"displayOperationId"`

	// The default expansion depth for Models (set to -1 completely hide the Models).
	DefaultModelsExpandDepth *int `cfg:"defaultModelsExpandDepth"`

	// The default expansion depth for the model on the model-example section.
	DefaultModelExpandDepth *int `cfg:"defaultModelExpandDepth"`

	// Controls how the model is shown when the API is first rendered. (The user can always switch the rendering
	// for a given model by clicking the 'Model' and 'Example Value' links.)
	// @Available: ["example", "model"]
	DefaultModelRendering string `cfg:"defaultModelRendering"`

	// Controls the display of the request duration (in milliseconds) for "Try it out" requests.
	DisplayRequestDuration *bool `cfg:"displayRequestDuration"`

	// Controls the default expansion setting for the operations and tags. It can be 'list' (expands only the tags),
	// 'full' (expands the tags and operations) or 'none' (expands nothing).
	// @Available: ["list", "full", "none"]
	DocExpansion string `cfg:"docExpansion"`

	// If set, enables filtering. The top bar will show an edit box that you can use to filter the tagged
	// operations that are shown. Can be Boolean to enable or disable, or a string, in which case filtering will
	// be enabled using that string as the filter expression. Filtering is case sensitive matching the filter
	// expression anywhere inside the tag
	Filter string `cfg:"filter"`

	// If set, limits the number of tagged operations displayed to at most this many.
	// The default is to show all operations.
	MaxDisplayedTags *int `cfg:"maxDisplayedTags"`

	// Controls the display of vendor extension (x-) fields and values for Operations, Parameters,
	// Responses, and Schema.
	ShowExtensions *bool `cfg:"showExtensions"`

	// Controls the display of extensions (pattern, maxLength, minLength, maximum, minimum) fields and values
	// for Parameters.
	ShowCommonExtensions *bool `cfg:"showCommonExtensions"`

	// Deprecated: This parameter is Deprecated and will be removed in 4.0.0.
	// When enabled, sanitizer will leave style, class and data-* attributes untouched on all HTML Elements
	// declared inside markdown strings. This parameter is Deprecated and will be removed in 4.0.0.
	UseUnsafeMarkdown *bool `cfg:"useUnsafeMarkdown"`

	// Set to false to deactivate syntax highlighting of payloads and cURL command, can be otherwise an object
	// with the activate and theme properties.
	SyntaxHighlight SyntaxHighlight `cfg:"syntaxHighlight"`

	// Controls whether the "Try it out" section should be enabled by default.
	TryItOutEnabled *bool `cfg:"tryItOutEnabled"`

	// Enables the request snippet section. When disabled, the legacy curl snippet will be used.
	RequestSnippetsEnabled *bool `cfg:"requestSnippetsEnabled"`

	/*
		Network
	*/

	// OAuth redirect URL.
	Oauth2RedirectURL string `cfg:"oauth2RedirectUrl"`

	// If set, MUST be an array of command line options available to the curl command.
	// This can be set on the mutated request in the requestInterceptor function.
	// For example request.curlOptions = ["-g", "--limit-rate 20k"]
	RequestCurlOptions []string `cfg:"request.curlOptions"`

	// If set to true, uses the mutated request returned from a requestInterceptor to produce the curl command
	// in the UI, otherwise the request before the requestInterceptor was applied is used.
	ShowMutatedRequest *bool `cfg:"showMutatedRequest"`

	// List of HTTP methods that have the "Try it out" feature enabled. An empty array disables "Try it out" for
	// all operations. This does not filter the operations from the display.
	// @Available: ["get", "put", "post", "delete", "options", "head", "patch", "trace"]
	SupportedSubmitMethods []string `cfg:"supportedSubmitMethods"`

	// By default, Swagger UI attempts to validate specs against swagger.io's online validator.
	// You can use this parameter to set a different validator URL, for example for locally deployed validators
	// (Validator Badge). Setting it to either none, 127.0.0.1 or localhost will disable validation.
	// @URL: https://github.com/swagger-api/validator-badge
	ValidatorURL string `cfg:"validatorUrl"`

	// If set to true, enables passing credentials, as defined in the Fetch standard, in CORS requests that are
	// sent by the browser. Note that Swagger UI cannot currently set cookies cross-domain (see swagger-js#1163) -
	// as a result, you will have to rely on browser-supplied cookies (which this setting enables sending)
	// that Swagger UI cannot control.
	WithCredentials *bool `cfg:"withCredentials"`

	/*
		Authorization
	*/

	// If set to true, it persists authorization data and it would not be lost on browser close/refresh.
	PersistAuthorization *bool `cfg:"persistAuthorization"`
}

type URL struct {
	URL  string `cfg:"url"`
	Name string `cfg:"name"`
}

type Item string

type Object map[string]any

type SyntaxHighlight struct {
	// Whether syntax highlighting should be activated or not.
	Activate *bool `cfg:"activate"`

	// Highlight.js syntax coloring theme to use. (Only these 6 styles are available.)
	// @URL: https://highlightjs.org/static/demo/
	// @Available: ["agate", "arta", "monokai", "nord", "obsidian", "tomorrow-night"]
	Theme string `cfg:"theme"`
}

func (c Config) Encode() []byte {
	buffer := new(bytes.Buffer)

	enc := newEncoder(buffer)
	enc.SetIndent("    ", "  ")

	buffer.WriteString("window.onload = function() {\n")
	buffer.WriteString("  window.ui = SwaggerUIBundle(\n")
	enc.Encode(c)
	buffer.WriteString("\n")
	buffer.WriteString("  )\n")
	buffer.WriteString("}\n")

	return buffer.Bytes()
}

func newEncoder(w io.Writer) *encoder {
	return &encoder{w: w}
}

type encoder struct {
	w io.Writer

	depth        int
	indentPrefix string
	indentValue  string
}

func (enc *encoder) Encode(v any) {
	enc.encode("", v)
}

func (enc *encoder) SetIndent(prefix, indent string) {
	enc.indentPrefix = prefix
	enc.indentValue = indent
}

func (enc *encoder) sub(depth int) *encoder {
	return &encoder{
		w:            enc.w,
		depth:        enc.depth + depth,
		indentPrefix: enc.indentPrefix,
		indentValue:  enc.indentValue,
	}
}

func (enc *encoder) write(value string) {
	_, _ = fmt.Fprintf(enc.w, "%s", value)
}

func (enc *encoder) prefix() {
	enc.write(enc.indentPrefix + strings.Repeat(enc.indentValue, enc.depth))
}

func (enc *encoder) encode(key string, value any) {
	switch to, vo := reflect.TypeOf(value), reflect.ValueOf(value); vo.Kind() {
	case reflect.Pointer:
		enc.encode(key, vo.Elem().Interface())
	case reflect.Struct:
		enc.prefix()

		if key != "" {
			enc.write(key + ": ")
		}

		enc.write("{\n")
		subEnc := enc.sub(1)

		for i, t := 0, to.NumField(); i < t; i++ {
			if !vo.Field(i).IsZero() {
				subEnc.encode(to.Field(i).Tag.Get("cfg"), vo.Field(i).Interface())
				subEnc.write(",\n")
			}
		}

		enc.prefix()
		enc.write("}")
	case reflect.Slice:
		if vo.Len() > 0 {
			enc.prefix()

			if key != "" {
				enc.write(key + ": ")
			}

			enc.write("[\n")
			subEnc := enc.sub(1)

			for i, t := 0, vo.Len(); i < t; i++ {
				subEnc.encode("", vo.Index(i).Interface())
				subEnc.write(",\n")
			}

			enc.prefix()
			enc.write("]")
		}
	case reflect.Map:
		if !vo.IsNil() && vo.Type() == reflect.TypeOf(Object{}) {
			if data, err := json.Marshal(vo.Interface()); err == nil {
				enc.prefix()

				if key != "" {
					enc.write(key + ": ")
				}

				enc.write(string(data))
			}
		}
	case reflect.String:
		enc.prefix()

		if key != "" {
			enc.write(key + ": ")
		}

		if vo.Type() == reflect.TypeOf(Item("")) {
			enc.write(vo.String())
		} else {
			enc.write("'" + vo.String() + "'")
		}
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		enc.prefix()

		if key != "" {
			enc.write(key + ": ")
		}

		enc.write(strconv.FormatInt(vo.Int(), 10))
	case reflect.Bool:
		enc.prefix()
		enc.write(key + ": " + strconv.FormatBool(vo.Bool()))
	}
}
